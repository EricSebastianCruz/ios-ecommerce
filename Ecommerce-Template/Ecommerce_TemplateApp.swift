//
//  Ecommerce_TemplateApp.swift
//  Ecommerce-Template
//
//  Created by PROGRAMAR on 02/04/21.
//

import SwiftUI

@main
struct Ecommerce_TemplateApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
